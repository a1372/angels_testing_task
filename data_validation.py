from pydantic import BaseModel, validator
from typing import List, Optional


BOOK_GENRES = ['fantasy', 'detective', 'history', 'other']


class CreateBook(BaseModel):
    """
    Book class to validate incoming list of books in body of a request
    """
    name: str
    author: str
    genre: str
    photo: bytes
    price: float

    @validator('genre')
    def genre_match(cls, v):
        if v not in BOOK_GENRES:
            return 'other'
        else:
            return v


class CreateBooks(BaseModel):
    """
    Class to validate json of a request body on create books
    """
    Books: List[CreateBook]


class FindBooks(BaseModel):
    name: Optional[str] = None
    author: Optional[str] = None
    genre: Optional[str] = None
    price_from: Optional[float] = None
    price_to: Optional[float] = None

    @validator('genre')
    def genre_match(cls, v):
        if v is None:
            return v
        if v in BOOK_GENRES:
            return v
