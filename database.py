from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


DEFAULT_URL = 'postgresql+asyncpg://root:root@localhost:5432/testing_shit'

ENGINE = create_async_engine(DEFAULT_URL, echo=False)

AsyncSessionMaker = sessionmaker(
    ENGINE,
    expire_on_commit=False,
    class_=AsyncSession
)

Base = declarative_base()
