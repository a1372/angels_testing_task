# API интернет магазина

## Описание
API выполнено с использованием веб фреймворка FastAPI и ORM SQLAlchemy

Реализует два post метода:
- add_books - создание запесей в бд. На вход принимает json со списком книг. Все параметры обязательны.

    Json example:
    ```python
        {
          "Books": [
            {
              "name": "python manual",
              "author": "Mark Lutz",
              "genre": "some genre",
              "photo": "some bytes",
              "price": 999999
            }
          ]
        }
    ```

- find_books - возвращает список книг по фильтру. Фильтр указывается в json. Все поля опциональны.
Отсутствие фильтра вернет список всех книг. 

    Json example:
    ```python
        {
          "name": "python",
          "author": "Mark Lutz",
          "genre": "some genre",
          "price_from": 0,
          "price_to": 999999
        }
    ```
  
## Требования
- Python 3.6.9+
- база данных PostgreSQL

## Установка
```
git clone https://gitlab.com/a1372/angels_testing_task.git
cd angels_testing_task
pip install -r requirements.txt
```

Изменить настройки подключения к базе данных (url) в database.py.

```python
DEFAULT_URL = 'postgresql://root:root@localhost:5432/testing_shit'
```

## Запуск
```
uvicorn main:app --port 8002
```

Добавь '--reload' для авторестарта

Swagger doc будет доступен по адресу http://127.0.0.1:8002/docs