from fastapi import FastAPI, Depends
from pydantic import BaseSettings
from sqlalchemy import select
from sqlalchemy.orm import Session
from database import AsyncSessionMaker
from data_validation import CreateBooks, FindBooks
import db_models


class Settings(BaseSettings):
    """
    Some settings
    """
    app_name = "Testing Task API"


settings = Settings()
app = FastAPI()


async def get_db():
    """
    Creates session with database
    """
    session = AsyncSessionMaker()
    try:
        yield session
    finally:
        await session.close()


@app.post('/add_books/')
async def add_books(books_lst: CreateBooks, session: Session = Depends(get_db)):
    """
    Endpoint to create new book rows in database
    :param books_lst: instance of CreateBooks validator
    :param session: session instance
    :return: json response
    """
    for book_serial in books_lst.Books:
        book_inst = db_models.Book(**book_serial.dict())
        session.add(book_inst)
    await session.commit()
    return {'success': True}


@app.post('/find_books/')
async def get_books(book_filter: FindBooks, session: Session = Depends(get_db)):
    """
    Endpoint to find books in database using filters
    :param book_filter: instance of FindBooks validator
    :param session: session instance
    :return: json response with books
    """
    query = select(db_models.Book)

    # filter by genre
    if book_filter.genre:
        query = query.filter(db_models.Book.genre == book_filter.genre)

    # filter if cheaper or eq than
    if book_filter.price_to:
        query = query.filter(db_models.Book.price <= book_filter.price_to)

    # filter if more expensive than
    if book_filter.price_from:
        query = query.filter(db_models.Book.price >= book_filter.price_from)

    # filter by author name
    if book_filter.author:
        query = query.filter(db_models.Book.author.contains(book_filter.author))

    # filter by book name
    if book_filter.name:
        query = query.filter(db_models.Book.name.contains(book_filter.name))

    # execute select
    books = await session.execute(query)

    # make response
    response = {'Books': []}
    for book, *_ in books:
        response['Books'].append(book.dict())

    return response
