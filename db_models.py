import enum
import asyncio
from database import Base, ENGINE
from sqlalchemy import Column, Integer, String, LargeBinary, Enum, Float


class Book(Base):

    """
    DB model for books

    Needs to have all columns filled to create a row
    :name: str
    :genre: enum.Enum
    :photo: bytes
    :price: int
    """

    __tablename__ = 'books'

    class Genre(enum.Enum):
        fantasy = 'fantasy'
        detective = 'detective'
        history = 'history'
        other = 'other'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=255), nullable=False)
    author = Column(String(length=255), nullable=False)
    genre = Column(Enum(Genre), nullable=False)
    photo = Column(LargeBinary, nullable=False)
    price = Column(Float, nullable=False)

    def dict(self):
        """
        :return: dict representation of a class values
        """
        return self.__dict__

    def __repr__(self):
        """
        :return: str representation of a class
        """
        class_name = 'Book'
        columns_values = ''
        for c in self.__table__.columns:
            columns_values += f'{c.name}: {getattr(self, c.name)}, '
        repr_str = f'(<{class_name} | {columns_values[:-2]}>)'
        return repr_str


async def create_all():
    """
    Async function to create all
    :return: True on success else False
    """

    try:
        async with ENGINE.begin() as connection:
            await connection.run_sync(Base.metadata.create_all)
        return True
    except Exception as ex:
        print(ex)
        return False


async def drop_all():
    """
    Async func to drop all
    :return: True on success else False
    """
    try:
        async with ENGINE.begin() as connection:
            await connection.run_sync(Base.metadata.drop_all)
        return True
    except Exception as ex:
        print(ex)
        return False


def async_wrap(func):

    def wrapper():
        features = [func()]
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(features))

    return wrapper()


if __name__ == '__main__':
    async_wrap(create_all)
    # async_wrap(drop_all)
